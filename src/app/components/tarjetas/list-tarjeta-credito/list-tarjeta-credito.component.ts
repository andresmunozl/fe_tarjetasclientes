import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TarjetaCredito } from 'src/app/models/tarjetaCredito';
import { TarjetaService } from 'src/app/services/tarjeta.service';

@Component({
  selector: 'app-list-tarjeta-credito',
  templateUrl: './list-tarjeta-credito.component.html',
  styleUrls: ['./list-tarjeta-credito.component.css']
})
export class ListTarjetaCreditoComponent implements OnInit {

  constructor(public tarjetaService: TarjetaService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.tarjetaService.obtenerTarjetas();
  }

  eliminarTarjeta(id: any){
    if(confirm('Esat seguro de eliminar los datos de la tarjeta?')){
      this.tarjetaService.eliminarTarjeta(id).subscribe(data => {
        this.toastr.warning("Se eliminó la tarjeta correctamente", "Tarjeta Eliminada")
        this.tarjetaService.obtenerTarjetas();
      })
    }
  }

  editarTarjeta(tarjeta: TarjetaCredito){
    // alert(tarjeta.titular);

    this.tarjetaService.actualizar(tarjeta)
  }

}
