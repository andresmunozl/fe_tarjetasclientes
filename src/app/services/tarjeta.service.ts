import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { TarjetaCredito } from '../models/tarjetaCredito';

@Injectable({
  providedIn: 'root'
})
export class TarjetaService {

  urlApp = 'https://localhost:44375/'; //local
  // urlApp = 'https://beclientesservice.azurewebsites.net/';
  urlApi = 'api/TarjetaClientes/';

  private actualizarFormulario = new BehaviorSubject<TarjetaCredito>({} as any)

  list?: TarjetaCredito[];
  constructor(private http: HttpClient) { }

  guardarTarjeta(tarjeta: TarjetaCredito): Observable<TarjetaCredito> {
    return  this.http.post(this.urlApp+this.urlApi, tarjeta)
  }

  obtenerTarjetas(){
    return this.http.get(this.urlApp+this.urlApi).subscribe(data => {
      this.list = data as TarjetaCredito[];
    })
  }

  eliminarTarjeta(id: any): Observable<TarjetaCredito> {
    return this.http.delete<TarjetaCredito>(this.urlApp+this.urlApi+id)
  }

  actualizar(tarjeta: any){

    this.actualizarFormulario.next(tarjeta);
  }
  obtenerTarjeta(): Observable<TarjetaCredito> {
    return this.actualizarFormulario.asObservable();
  }

  modificartarjeta(id: number, tarjeta: TarjetaCredito): Observable<TarjetaCredito>{
    return this.http.put(this.urlApp+this.urlApi+id, tarjeta)
  }
}
